@echo off

REM SET CompilerFlags=/nologo /Zi /fp:fast /Wall /W1
SET LinkerFlags=/INCREMENTAL:no

REM Release settings
SET CompilerFlags=/nologo /Oi /Ox /fp:fast /favor:INTEL64 /w /MT

if not exist ..\build mkdir ..\build
pushd ..\build

cl %CompilerFlags% ..\code\main.cpp /link %LinkerFlags%

popd
