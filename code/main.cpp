#include <stdio.h>
#include <stdlib.h>

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))
#define true 1
#define false 0

typedef int bool32;

static inline int
IsWhitespace(char Character)
{
	int Result = ((Character == ' ') ||
				  (Character == '\t') ||
				  (Character == '\n'));
	return Result;
}

static inline int
IsDigit(char Character)
{
	int Result = ((Character >= '0') && (Character <= '9'));
	return Result;
}

static inline int
StringFind(char *Haystack, char *Needle)
{
	int Result = false;

	while((*Haystack != 0) && (Result != true))
	{
		char *HaystackChar = Haystack;
		char *NeedleChar = Needle;
		for(; ((*HaystackChar == *NeedleChar) &&
			   (*HaystackChar != 0) &&
			   (*NeedleChar != 0));
			++HaystackChar, ++NeedleChar);
		if(*NeedleChar == 0)
		{
			Result = true;
		}

		++Haystack;
	}

	return Result;
}

static inline int
IsAlpha(char Character)
{
	int Result = (((Character >= 'a') && (Character <= 'z')) ||
				  ((Character >= 'A') && (Character <= 'Z')));

	return Result;
}

struct table_of_contents_item
{
	char *NumbersStart;
	int NumbersLength;

	char *NameStart;
	int NameLength;

	int FillLength;
	int IndentAmount;
};

static inline struct table_of_contents_item
GetTableOfContentsItem(char *Line, char NextChar)
{
	struct table_of_contents_item Result = {0};
	bool32 InNumbers = true;
	bool32 InTitle = false;

	Result.NumbersStart = Line;
	while((*Line != 0) && (*Line != '\n'))
	{
		// NOTE(rick): This is sort-of like a statemachine where we'll switch
		// the context of what we're parsing and the switch will determine how
		// we pull things out of the line.
		if((InNumbers == true) && ((!IsDigit(*Line) && (*Line != '.'))))
		{
			Result.NumbersLength = Line - Result.NumbersStart;
			InNumbers = false;
		}
		else if((InTitle == false) && (IsAlpha(*Line)))
		{
			Result.NameStart = Line;
			InTitle = true;
		}

		++Line;
	}
	Result.NameLength = Line - Result.NameStart;

	if(NextChar == '-')
	{
		Result.IndentAmount = 4;
	}
	else if(NextChar != '=')
	{
		Result.IndentAmount = 8;
	}

	int LineLength = 80;
	Result.FillLength = LineLength - Result.NumbersLength - Result.NameLength -
		Result.IndentAmount - 2;

	return Result;
}

static void
PrintTableOfContentsLine(struct table_of_contents_item Toc)
{
	for(int Indent = 0; Indent < Toc.IndentAmount; ++Indent)
	{
		fputc(' ', stdout);
	}

	fprintf(stdout, "%.*s ", Toc.NameLength, Toc.NameStart);
	for(int Fill = 0; Fill < Toc.FillLength; ++Fill)
	{
		fputc('.', stdout);
	}

	fprintf(stdout, " %.*s\n", Toc.NumbersLength, Toc.NumbersStart);
}

enum processing_mode
{
	ProcessingMode_TOC = 0,
	ProcessingMode_Client,

	ProcessingMode_Undefined = -1,
};

static inline int
StrCmp(char *Left, char *Right)
{
	int Result = 0;

	while((*Left == *Right) && (*Left != 0))
	{
		++Left, ++Right;
	}
	Result = (*Left - *Right);

	return Result;
}

int main(int argc, char **argv)
{
	processing_mode Mode = ProcessingMode_Undefined;
	if(argc > 1)
	{
		if((StrCmp(argv[1], "-h") == 0) ||
		   (StrCmp(argv[1], "--help") == 0))
		{
			printf("Usage: %s <option>\n", argv[0]);
			printf("\t-h\t\tPrints this help message\n");
			printf("\t-toc\t\tCreates a table of contents\n");
			printf("\t-client\tRemoves details section\n");
			return 1;
		}
		else if(StrCmp(argv[1], "-toc") == 0)
		{
			Mode = ProcessingMode_TOC;
		}
		else if(StrCmp(argv[1], "-client") == 0)
		{
			Mode = ProcessingMode_Client;
		}
	}

	char ReadBuffer[256] = {0};
	while((fgets(ReadBuffer, ArrayCount(ReadBuffer), stdin)) != 0)
	{
		char *Character = ReadBuffer;
		if((Mode == ProcessingMode_TOC) && (IsDigit(*Character)))
		{
			// NOTE(rick): Get the next character so we know what kind of
			// indentation we need
			int NextChar = fgetc(stdin);
			ungetc(NextChar, stdin);

			struct table_of_contents_item Toc = GetTableOfContentsItem(Character, NextChar);
			PrintTableOfContentsLine(Toc);
		}
		else if((Mode == ProcessingMode_Client) && (StringFind(ReadBuffer, "Details:")))
		{
			while((fgets(ReadBuffer, ArrayCount(ReadBuffer), stdin)) != 0)
			{
				if(StringFind(ReadBuffer, "Client Friendly:"))
				{
					break;
				}
			}
		}

		if(Mode != ProcessingMode_TOC)
		{
			while(*Character != 0)
			{
				// Convert tabs to spaces
				if(*Character == '\t')
				{
					fputs("    ", stdout);
				}
				else
				{
					fputc(*Character, stdout);
				}
				++Character;
			}
		}

	}

	return 0;
}
